package teck.me.license;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import teck.me.license.exception.NotFoundException;
import teck.me.license.model.CryptoKey;
import teck.me.license.model.Customer;
import teck.me.license.model.License;
import teck.me.license.model.Project;
import teck.me.license.model.dto.CreateLicenseDto;
import teck.me.license.model.dto.LicenseDto;
import teck.me.license.repository.LicenseRepository;
import teck.me.license.service.imp.CryptoKeyServiceImp;
import teck.me.license.service.imp.CustomerServiceImp;
import teck.me.license.service.imp.LicenseServiceImp;
import teck.me.license.service.imp.ProjectServiceImp;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SpringBootTest
class LicenseServiceImpTest {

    @Autowired
    private LicenseServiceImp licenseService;

    @MockBean
    private LicenseRepository licenseRepository;

    @MockBean
    private CryptoKeyServiceImp cryptoKeyServiceImp;

    @MockBean
    private CustomerServiceImp customerServiceImp;

    @MockBean
    private ProjectServiceImp projectServiceImp;

    @BeforeEach
    void setUp() {
        // Mocking behavior for findAll method
        List<License> licenses = new ArrayList<>();
        licenses.add(new License(30, 1644409800L, "Description 1", new CryptoKey(), new Project(), new Customer(), new HashMap<>()));
        licenses.add(new License(60, 1644409800L, "Description 2", new CryptoKey(), new Project(), new Customer(), new HashMap<>()));
        Page<License> licensePage = new PageImpl<>(licenses);
        when(licenseRepository.findAll(any(Pageable.class))).thenReturn(licensePage);

        // Mocking behavior for findByUuid method
        License license1 = new License(30, 1644409800L, "Description 1", new CryptoKey(), new Project(), new Customer(), new HashMap<>());
        license1.setUuid("UUID1");
        licenseRepository.save(license1);
        when(licenseRepository.findByUuid("UUID1")).thenReturn(Optional.of(license1));

        // Mocking behavior for existsByUuid method
        when(licenseRepository.existsByUuid("UUID1")).thenReturn(true);
        when(licenseRepository.existsByUuid("NonExistentUUID")).thenReturn(false);

        // Mocking behavior for deleteByUuid method
        doNothing().when(licenseRepository).deleteByUuid("UUID1");

        // Mocking behavior for getProject method
        Project project1 = new Project("Project1", "Description1", new ArrayList<>(), new ArrayList<>());
        when(projectServiceImp.getProject("Project1")).thenReturn(project1);

        // Mocking behavior for getCustomer method
        Customer customer1 = new Customer("Customer1", "email@example.com", "09116639852", "Address");
        when(customerServiceImp.getCustomer("Customer1")).thenReturn(customer1);

        // Mocking behavior for getCryptoKey method
        CryptoKey cryptoKey1 = new CryptoKey("Description1", project1, new ArrayList<>());
        when(cryptoKeyServiceImp.getCryptoKey("UUID1")).thenReturn(cryptoKey1);

        // Mocking behavior for saveCryptoKey method
        doNothing().when(cryptoKeyServiceImp).saveCryptoKey(any(CryptoKey.class));
    }


    @Test
    void createLicense() {
        CreateLicenseDto newLicenseDto = new CreateLicenseDto(30, 1644409800L, "UUID1", "Project1", "Customer1", new HashMap<>(), "New Description");
        CreateLicenseDto result = licenseService.createLicense(newLicenseDto, "UUID1", "Project1", "Customer1");
        assertEquals(newLicenseDto.getDescription(), result.getDescription());
    }

    @Test
    void getAllLicenses() {
        List<LicenseDto> result = licenseService.getAllLicenses(0, 10);
        assertEquals(2, result.size());
        assertEquals("Description 1", result.get(0).getDescription());
        assertEquals("Description 2", result.get(1).getDescription());
    }

    @Test
    void getLicenseById() {
        LicenseDto result = licenseService.getLicenseById("UUID1");
        assertEquals("Description 1", result.getDescription());
    }

    @Test
    void getLicenseById_NotFound() {
        assertThrows(NotFoundException.class, () -> licenseService.getLicenseById("NonExistentUUID"));
    }

    @Test
    void updateLicense() {
        CreateLicenseDto updatedLicenseDto = new CreateLicenseDto(30, 1644409800L, "UUID1", "Project1", "Customer1", new HashMap<>(), "Updated Description");
        CreateLicenseDto result = licenseService.updateLicense("UUID1", updatedLicenseDto);
        assertEquals(updatedLicenseDto, result);
    }

    @Test
    void deleteLicense() {
        assertDoesNotThrow(() -> licenseService.deleteLicense("UUID1"));
        verify(licenseRepository, times(1)).deleteByUuid("UUID1");
    }
}
