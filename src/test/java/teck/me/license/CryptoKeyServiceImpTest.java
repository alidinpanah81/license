package teck.me.license;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import teck.me.license.exception.NotFoundException;
import teck.me.license.model.CryptoKey;
import teck.me.license.model.Project;
import teck.me.license.model.dto.CreateCryptoKeyDto;
import teck.me.license.model.dto.CryptoKeyDto;
import teck.me.license.model.dto.ListCryptoKeyDto;
import teck.me.license.repository.CryptoKeyRepository;
import teck.me.license.service.imp.CryptoKeyServiceImp;
import teck.me.license.service.imp.ProjectServiceImp;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SpringBootTest
class CryptoKeyServiceImpTest {

    @Autowired
    private CryptoKeyServiceImp cryptoKeyService;

    @MockBean
    private CryptoKeyRepository cryptoKeyRepository;

    @MockBean
    private ProjectServiceImp projectServiceImp;



    @BeforeEach
    void setUp() {
        // Mocking behavior for findAll method
//        projectService.createProject(new CreateProjectDto("Project1","Description",new ArrayList<>(),new ArrayList<>()));
        Project project1=new Project("Project1","Description1",new ArrayList<>(),new ArrayList<>());
        Project project2=new Project("Project2","Description2",new ArrayList<>(),new ArrayList<>());
        projectServiceImp.saveProject(project1);
        projectServiceImp.saveProject(project2);
        List<CryptoKey> cryptoKeys = new ArrayList<>();
        cryptoKeys.add(new CryptoKey("Description 1",project1 , new ArrayList<>()));
        cryptoKeys.add(new CryptoKey("Description 2", project2, new ArrayList<>()));
        Page<CryptoKey> cryptoKeyPage = new PageImpl<>(cryptoKeys);
        when(cryptoKeyRepository.findAll(any(Pageable.class))).thenReturn(cryptoKeyPage);

        // Mocking behavior for findByUuid method
        CryptoKey cryptoKey1 = new CryptoKey("Description 1",project1, new ArrayList<>());
        when(cryptoKeyRepository.findByUuid("UUID1")).thenReturn(Optional.of(cryptoKey1));

        // Mocking behavior for existsByUuid method
        when(cryptoKeyRepository.existsByUuid("UUID1")).thenReturn(true);
        when(cryptoKeyRepository.existsByUuid("NonExistentUUID")).thenReturn(false);

        // Mocking behavior for deleteByUuid method
        doNothing().when(cryptoKeyRepository).deleteByUuid("UUID1");
    }

    @Test
    void createCryptoKey() {
        CreateCryptoKeyDto newCryptoKeyDto = new CreateCryptoKeyDto("Project1", "New Description", new ArrayList<>());
        CreateCryptoKeyDto result = cryptoKeyService.createCryptoKey(newCryptoKeyDto);
        assertEquals(newCryptoKeyDto.getDescription(), result.getDescription());
    }

    @Test
    void getAllCryptoKeys() {
        List<ListCryptoKeyDto> result = cryptoKeyService.getAllCryptoKeys(0, 10);
        assertEquals(2, result.size());
        assertEquals("Description 1", result.get(0).getDescription());
        assertEquals("Description 2", result.get(1).getDescription());
    }

    @Test
    void getCryptoKeyById() {
        CryptoKeyDto result = cryptoKeyService.getCryptoKeyById("UUID1");
        assertEquals("Description 1", result.getDescription());
    }

    @Test
    void getCryptoKeyById_NotFound() {
        assertThrows(NotFoundException.class, () -> cryptoKeyService.getCryptoKeyById("NonExistentUUID"));
    }

    @Test
    void updateCryptoKey() {
        CreateCryptoKeyDto updatedCryptoKeyDto = new CreateCryptoKeyDto("Project1", "Updated Description", new ArrayList<>());
        CreateCryptoKeyDto result = cryptoKeyService.updateCryptoKey("UUID1", updatedCryptoKeyDto);
        assertEquals(updatedCryptoKeyDto, result);
    }

    @Test
    void deleteCryptoKey() {
        assertDoesNotThrow(() -> cryptoKeyService.deleteCryptoKey("UUID1"));
        verify(cryptoKeyRepository, times(1)).deleteByUuid("UUID1");
    }

}

