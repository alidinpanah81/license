package teck.me.license;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import teck.me.license.exception.ConflictException;
import teck.me.license.exception.NotFoundException;

import teck.me.license.model.Project;
import teck.me.license.model.dto.CreateProjectDto;
import teck.me.license.model.dto.ListProjectDto;
import teck.me.license.model.dto.ProjectDto;
import teck.me.license.repository.ProjectRepository;
import teck.me.license.service.imp.ProjectServiceImp;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SpringBootTest
class ProjectServiceImpTest {

    @Autowired
    private ProjectServiceImp projectService;

    @MockBean
    private ProjectRepository projectRepository;

    @BeforeEach
    void setUp() {
        // Mocking behavior for findAll method
        List<Project> projects = new ArrayList<>();
        projects.add(new Project("Project1", "Description 1", new ArrayList<>(), new ArrayList<>()));
        projects.add(new Project("Project2", "Description 2", new ArrayList<>(), new ArrayList<>()));
        Page<Project> projectPage = new PageImpl<>(projects);
        when(projectRepository.findAll(any(Pageable.class))).thenReturn(projectPage);

        // Mocking behavior for findById method
        Project project1 = new Project("Project1", "Description 1", new ArrayList<>(), new ArrayList<>());
        when(projectRepository.findByName("Project1")).thenReturn(Optional.of(project1));

        // Mocking behavior for existsByName method
        when(projectRepository.existsByName("Project1")).thenReturn(true);
        when(projectRepository.existsByName("NewProject")).thenReturn(false);

        // Mocking behavior for deleteByName method
        doNothing().when(projectRepository).deleteByName("Project1");
    }

    @Test
    void createProject() {
        CreateProjectDto newProjectDto = new CreateProjectDto("NewProject", "New Description", new ArrayList<>(), new ArrayList<>());
        CreateProjectDto result = projectService.createProject(newProjectDto);
        assertEquals(newProjectDto, result);
    }

    @Test
    void createProject_Conflict() {
        CreateProjectDto existingProjectDto = new CreateProjectDto("Project1", "Description 1", new ArrayList<>(), new ArrayList<>());
        assertThrows(ConflictException.class, () -> projectService.createProject(existingProjectDto));
    }

    @Test
    void getAllProjects() {
        List<ListProjectDto> result = projectService.getAllProjects(0, 10);
        assertEquals(2, result.size());
        assertEquals("Project1", result.get(0).getName());
        assertEquals("Project2", result.get(1).getName());
    }

    @Test
    void getProjectById() {
        ProjectDto result = projectService.getProjectById("Project1");
        assertEquals("Project1", result.getName());
        assertEquals("Description 1", result.getDescription());
    }

    @Test
    void getProjectById_NotFound() {
        assertThrows(NotFoundException.class, () -> projectService.getProjectById("NonExistentProject"));
    }

    @Test
    void updateProject() {
        CreateProjectDto updatedProjectDto = new CreateProjectDto("Project1", "Updated Description", new ArrayList<>(), new ArrayList<>());
        CreateProjectDto result = projectService.updateProject("Project1", updatedProjectDto);
        assertEquals(updatedProjectDto, result);
    }

    @Test
    void updateProject_Conflict() {
        CreateProjectDto updatedProjectDto = new CreateProjectDto("Project1", "Updated Description", new ArrayList<>(), new ArrayList<>());
        when(projectRepository.findByName("NewProject")).thenReturn(Optional.of(new Project()));
        assertThrows(ConflictException.class, () -> projectService.updateProject("NewProject", updatedProjectDto));
    }

    @Test
    void updateProject_NotFound() {
        CreateProjectDto updatedProjectDto = new CreateProjectDto("NonExistentProject", "Updated Description", new ArrayList<>(), new ArrayList<>());
        assertThrows(NotFoundException.class, () -> projectService.updateProject("NonExistentProject", updatedProjectDto));
    }

    @Test
    void deleteProject() {
        assertDoesNotThrow(() -> projectService.deleteProject("Project1"));
        verify(projectRepository, times(1)).deleteByName("Project1");
    }
}

