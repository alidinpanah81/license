package teck.me.license;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import teck.me.license.exception.ConflictException;
import teck.me.license.exception.NotFoundException;
import teck.me.license.model.Customer;
import teck.me.license.model.dto.CreateCustomerDto;
import teck.me.license.model.dto.CustomerDto;
import teck.me.license.model.dto.ListCustomerDto;
import teck.me.license.repository.CustomerRepository;
import teck.me.license.service.imp.CustomerServiceImp;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SpringBootTest
class CustomerServiceImpTest {

    @Autowired
    private CustomerServiceImp customerService;

    @MockBean
    private CustomerRepository customerRepository;

    @BeforeEach
    void setUp() {
        // Mocking behavior for findAll method
        List<Customer> customers = new ArrayList<>();
        customers.add(new Customer("John", "john@example.com", "123456789", "Address 1"));
        customers.add(new Customer("Jane", "jane@example.com", "987654321", "Address 2"));
        Page<Customer> customerPage = new PageImpl<>(customers);
        when(customerRepository.findAll(any(Pageable.class))).thenReturn(customerPage);

        // Mocking behavior for findById method
        Customer john = new Customer("John", "john@example.com", "123456789", "Address 1");
        when(customerRepository.findByName("John")).thenReturn(Optional.of(john));

        // Mocking behavior for existsByName method
        when(customerRepository.existsByName("John")).thenReturn(true);
        when(customerRepository.existsByName("NewCustomer")).thenReturn(false);

        // Mocking behavior for deleteByName method
        doNothing().when(customerRepository).deleteByName("John");
    }

    @Test
    void getAllCustomers() {
        List<ListCustomerDto> result = customerService.getAllCustomers(0, 10);
        assertEquals(2, result.size());
        assertEquals("John", result.get(0).getName());
        assertEquals("Jane", result.get(1).getName());
    }

    @Test
    void getCustomerById() {
        CustomerDto result = customerService.getCustomerById("John");
        assertEquals("John", result.getName());
        assertEquals("john@example.com", result.getEmail());
        assertEquals("123456789", result.getPhoneNumber());
        assertEquals("Address 1", result.getAddress());
    }

    @Test
    void getCustomerById_NotFound() {
        assertThrows(NotFoundException.class, () -> customerService.getCustomerById("NonExistentCustomer"));
    }

    @Test
    void createCustomer() {
        CreateCustomerDto newCustomerDto = new CreateCustomerDto("NewCustomer", "new@example.com", "987654321", "New Address", new ArrayList<>());
        CreateCustomerDto result = customerService.createCustomer(newCustomerDto);
        assertEquals(newCustomerDto, result);
    }

    @Test
    void createCustomer_Conflict() {
        CreateCustomerDto existingCustomerDto = new CreateCustomerDto("John", "john@example.com", "123456789", "Address 1", new ArrayList<>());
        assertThrows(ConflictException.class, () -> customerService.createCustomer(existingCustomerDto));
    }

    @Test
    void updateCustomer() {
        CreateCustomerDto updatedCustomerDto = new CreateCustomerDto("John", "john.updated@example.com", "987654321", "Updated Address", new ArrayList<>());
        CreateCustomerDto result = customerService.updateCustomer("John", updatedCustomerDto);
        assertEquals(updatedCustomerDto, result);
    }

    @Test
    void updateCustomer_Conflict() {
        CreateCustomerDto updatedCustomerDto = new CreateCustomerDto("John", "john.updated@example.com", "987654321", "Updated Address", new ArrayList<>());
        when(customerRepository.findByName("NewCustomer")).thenReturn(Optional.of(new Customer()));
        assertThrows(ConflictException.class, () -> customerService.updateCustomer("NewCustomer", updatedCustomerDto));
    }

    @Test
    void updateCustomer_NotFound() {
        CreateCustomerDto updatedCustomerDto = new CreateCustomerDto("NonExistentCustomer", "john.updated@example.com", "987654321", "Updated Address", new ArrayList<>());
        assertThrows(NotFoundException.class, () -> customerService.updateCustomer("NonExistentCustomer", updatedCustomerDto));
    }

    @Test
    void deleteCustomer() {
        assertDoesNotThrow(() -> customerService.deleteCustomer("John"));
        verify(customerRepository, times(1)).deleteByName("John");
    }

}

